# Device tree for Xiaomi Redmi Note 10 Pro/Pro Max (codenamed _sweet_ / _sweetin_)


## Device specifications

| Device                  | Redmi Note 10 Pro/Pro Max                                   |
| ----------------------- | :---------------------------------------------------------- |
| SoC     		  | Qualcomm SM7150 Snapdragon 732G                             |
| CPU     		  | Octa-core (2x2.3 GHz Kryo 470 Gold & 6x1.8 GHz Kryo 470 Silver) |
| GPU     		  | Adreno 618                                                  |
| Memory                  | 6GB / 8GB RAM                                               |
| Shipped Android Version | 11 (MIUI 12)                                                |
| Storage                 | 64GB / 128GB                                                |
| MicroSD                 | MicroSDXC                                                   |
| Battery 		  | Li-Po 5020 mAh, non-removable                               |
| Display 		  | AMOLED, 120Hz, HDR10, 450 nits (typ), 6.67 inches, 1080 x 2400 pixels, 20:9 ratio (~395 ppi density) |

## Device picture

![Redmi Note 10 Pro](https://fdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-note10-pro-1.jpg "Redmi Note 10 Pro")

---
## Kernel sources
"https://github.com/vantoman/kernel_xiaomi_sm6150.git"

---
## Copyright notice
 ```
  /*
  *  Copyright (C) 2022 The OrangeFox Recovery Project
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */
  ```
